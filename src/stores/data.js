import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useDataStore = defineStore('data',{
    state:()=>{
        return{
            tableData: [
                {
                  id:"1",
                  telephone: "134123456",
                  name: "张三",
                  manager: "aaa",
                  level: 1,
                },
                {
                  id:"2",
                  telephone: "135123456",
                  name: "李四",
                  manager: "aaa",
                  level: 2,
                },
                {
                  id:"3",
                  telephone: "137123456",
                  name: "王五",
                  manager: "bbb",
                  level: 2,
                },
                {
                  id:"4",
                  telephone: "138123456",
                  name: "赵六",
                  manager: "bbb",
                  level: 2,
                },
                {
                  id:"5",
                  telephone: "138123456",
                  name: "田七",
                  manager: "bbb",
                  level: 1,
                },
                {
                  id:"6",
                  telephone: "130123456",
                  name: "翠花",
                  manager: "aaa",
                  level: 1,
                },
                {
                  id:"7",
                  telephone: "139223456",
                  name: "铁牛",
                  manager: "bbb",
                  level: 2,
                },
                {
                  id:"8",
                  telephone: "139323456",
                  name: "阿猫",
                  manager: "bbb",
                  level: 2,
                },
                {
                  id:"9",
                  telephone: "139423456",
                  name: "阿狗",
                  manager: "bbb",
                  level: 2,
                },
                {
                  id:"10",
                  telephone: "139523456",
                  name: "阿猪",
                  manager: "bbb",
                  level: 2,
                },
              ],
            newData_total:[],
        }
    },
    actions:{
        getData(){
            return this.newData
        },
        // 删除
        async delUser(row) {
            this.tableData.forEach((item, index) => {
                if (item.id=== row.id) {
                  this.tableData.splice(index, 1);
                }
            })
        },
        //增加
        addUser(count,Formname,Formtelephone,Formlevel,Formmanager){
            this.newData_total.push({
                id:count.toString(),
                name: Formname,
                telephone: Formtelephone,
                level: Formlevel,
                manager: Formmanager
              });
        },
        //修改
        eidtUser(index,Formname,Formtelephone,Formlevel,Formmanager){
            this.tableData[index].name=Formname,
            this.tableData[index].telephone=Formtelephone,
            this.tableData[index].level=Formlevel,
            this.tableData[index].manager=Formmanager
        },
        // 存量客户数
        getOldNumTotal(){
            return this.tableData.length
        },
        // 新增客户数
        getNewNumTotal(){
            return this.newData_total.length
        },
        // 存量客户中负责人aaa和bbb的1级客户和2级客户数量
        getOldLevelNum(){
            let a1 =0,a2=0;
            let b1 = 0,b2=0;
            this.tableData.map((item)=>{
                if(item.manager=='aaa'){
                    if(item.level==1) a1=a1+1;
                    else a2=a2+1;
                }
                if(item.manager=='bbb'){
                    if(item.level==1) b1=b1+1;
                    else b2=b2+1;
                }
            })
            return [a1,a2,b1,b2];
        },
        // 新增客户中负责人aaa和bbb的1级客户和2级客户数量
        getNewLevelNum(){
            let a1 =0,a2=0;
            let b1 = 0,b2=0;
            this.newData_total.map((item)=>{
                if(item.manager=='aaa'){
                    if(item.level==1) a1=a1+1;
                    else a2=a2+1;
                }
                if(item.manager=='bbb'){
                    if(item.level==1) b1=b1+1;
                    else b2=b2+1;
                }
            })
            return [a1,a2,b1,b2];
        }
    },
})
