import Mock from "mockjs";
import permissionApi from "./permission";

// 设置200-2000毫秒延时请求数据
// Mock.setup({
//   timeout: '200-2000'
// })


// 权限相关
Mock.mock(/\/permission\/getMenu/, "post", permissionApi.getMenu);
