import { makeInstaller } from 'element-plus'
import { createRouter, createWebHistory, createWebHashHistory } from 'vue-router'
import { useUserStore } from '../stores/user'

import Main from '../views/Main.vue'

const router = createRouter({
  // history: createWebHistory(import.meta.env.BASE_URL),
  history: createWebHashHistory(import.meta.env.BASE_URL),
  routes:[
    {
      path:"/login",
      name:"login",
      component:()=>import("../views/Login.vue")
    },
    {
      path:'/',
      component:Main,
      children:[
        {
          path:"/",
          redirect:'/home',
        },
        {
          path:'/home',
          name:'home',
          component: () => import('../views/Home.vue')
        },
        {
          path:'/about',
          name:'about',
          component: () => import('../views/About.vue')
        }
      ]
    }
  ]
})

router.beforeEach((to, from, next) => {
  const userStore=useUserStore();

  if (to.name !== 'login' && userStore.token=='') next({ name: 'login' })
  else next()
})


export default router
